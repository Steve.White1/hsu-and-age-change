set hsu_table = (select name from "DWH_PRD_SANDBOX"."COVID"."VACC_JF_HSU_POINTER");
set age_met = (select age from "DWH_PRD_SANDBOX"."COVID"."VACC_JF_HSU_POINTER");
with HSU as (SELECT AGE_IN_YEARS,
SUM(CNT_POP) pop
from identifier($hsu_table)
GROUP BY AGE_IN_YEARS),

partial as (SELECT identifier($age_met) page,
            count(distinct master_hcu_id) vpartial
            from "DWH_PRD_REP"."VACC"."SNP_CVIP_POPULATION_STATUS"
            where snapshot_date = current_date
            and vaccination_journey_status in ('Partially Vaccinated','Fully Vaccinated','Boosted')
            group by page),
            
fully as (SELECT identifier($age_met) fage,
            count(distinct master_hcu_id) vfully
            from "DWH_PRD_REP"."VACC"."SNP_CVIP_POPULATION_STATUS"
            where snapshot_date = current_date
            and vaccination_journey_status in ('Fully Vaccinated','Boosted')
            group by fage)

select *, vpartial/pop, vfully/pop from HSU
full outer join partial on HSU.AGE_IN_YEARS = partial.page
full outer join fully on partial.page = fully.fage;